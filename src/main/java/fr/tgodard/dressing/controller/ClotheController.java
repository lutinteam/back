package fr.tgodard.dressing.controller;

import fr.tgodard.dressing.beans.Clothe;
import fr.tgodard.dressing.service.ClotheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("clothes")
@CrossOrigin
public class ClotheController {

    private final ClotheService clotheService;

    @Autowired
    public ClotheController(ClotheService clotheService) {
        this.clotheService = clotheService;
    }

    @ResponseBody
    @RequestMapping()
    public Iterable<Clothe> getAllClothes() {
        return clotheService.getAllClothes();
    }

    @ResponseBody
    @RequestMapping("{id}")
    public Clothe getClothe(@PathVariable("id") Integer id) {
        return clotheService.getClothes(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void saveNewClothe(@RequestBody Clothe clothe) {
        if (clothe != null) {
            System.out.println(clothe);
            clotheService.saveClothe(clothe);
        } else {
            System.out.println("cloth null");
        }
    }

    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteClothe(@PathVariable("id") Integer id) {
        clotheService.deleteClothe(id);
    }
}
