package fr.tgodard.dressing.controller;

import fr.tgodard.dressing.service.FormDataService;
import fr.tgodard.dressing.viewModel.FormDataViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("formsdatas")
@CrossOrigin
public class FormDataController {

    private final FormDataService formDataService;

    @Autowired
    public FormDataController(FormDataService formDataService) {
        this.formDataService = formDataService;
    }

    /**
     * Send form datas in response to 'formsdata/clothes'
     * GET Method
     *
     */
    @ResponseBody
    @RequestMapping(path = "/clothes")
    public FormDataViewModel getFormDatas() {
        return formDataService.getFormDatas();
    }
}
