package fr.tgodard.dressing.beans;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"size", "size_type_id"})
)
public class Size {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String size;

    @ManyToOne(fetch = FetchType.LAZY)
    private SizeType sizeType;
}
