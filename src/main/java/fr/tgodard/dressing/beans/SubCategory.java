package fr.tgodard.dressing.beans;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"name", "category_id"})
)
public class SubCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private SizeType sizeType;

    private String name;
}
