package fr.tgodard.dressing.beans;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Clothe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Boolean isSummer;

    private Boolean isWinter;

    @ManyToOne(fetch = FetchType.LAZY)
    private Color firstColor;

    @ManyToOne(fetch = FetchType.LAZY)
    private Color secondColor;

    @ManyToOne(fetch = FetchType.LAZY)
    private Pattern pattern;

    private String brand;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    private SubCategory subCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    private Size size;

    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "cloth_id")
    private List<Photo> photos;
}
