package fr.tgodard.dressing.service;

import fr.tgodard.dressing.beans.Clothe;
import fr.tgodard.dressing.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ClotheService {

    private final IClotheRepository clotheRepository;

    @Autowired
    public ClotheService(IClotheRepository clotheRepository) {
        this.clotheRepository = clotheRepository;
    }

    public void saveClothe(Clothe clothe) {
        if (clothe.getPhotos() == null) clothe.setPhotos(new ArrayList<>());
        clotheRepository.save(clothe);
    }

    public Iterable<Clothe> getAllClothes() {
        return clotheRepository.findAllWithInfos();
    }

    public void deleteClothe(Integer id) {
        clotheRepository.deleteById(id);
    }

    public Clothe getClothes(Integer id) {
        return clotheRepository.findClotheByIdWithInfos(id);
    }
}
