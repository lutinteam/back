package fr.tgodard.dressing.service;

import fr.tgodard.dressing.repository.*;
import fr.tgodard.dressing.viewModel.FormDataViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormDataService {

    private final ICategoryRepository categoryRepository;
    private final IColorRepository colorRepository;
    private final ISizeRepository sizeRepository;
    private final IClotheRepository clotheRepository;
    private final IPatternRepository patternRepository;

    @Autowired
    public FormDataService(ICategoryRepository categoryRepository,
                           IColorRepository colorRepository,
                           ISizeRepository sizeRepository,
                           IClotheRepository clotheRepository,
                           IPatternRepository patternRepository) {
        this.categoryRepository = categoryRepository;
        this.colorRepository = colorRepository;
        this.sizeRepository = sizeRepository;
        this.clotheRepository = clotheRepository;
        this.patternRepository = patternRepository;
    }


    public FormDataViewModel getFormDatas() {
        FormDataViewModel newClotheForm = new FormDataViewModel();
        this.categoryRepository.findAllWithSubCategoriesAndSizeType().forEach(category -> newClotheForm.getCategories().add(category));
        this.colorRepository.findAll().forEach(color -> newClotheForm.getColors().add(color));
        this.sizeRepository.findAllWithSizeType().forEach(size -> newClotheForm.getSizes().add(size));
        this.patternRepository.findAll().forEach(pattern -> newClotheForm.getPatterns().add(pattern));
        this.clotheRepository.findAllBrands().forEach(brand -> newClotheForm.getBrands().add(brand));

        return newClotheForm;
    }
}
