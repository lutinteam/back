package fr.tgodard.dressing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DressingApplication {

    public static void main(String[] args) {
        SpringApplication.run(DressingApplication.class, args);
    }
}
