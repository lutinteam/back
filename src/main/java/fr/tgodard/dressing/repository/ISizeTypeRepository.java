package fr.tgodard.dressing.repository;

import fr.tgodard.dressing.beans.SizeType;
import org.springframework.data.repository.CrudRepository;

public interface ISizeTypeRepository extends CrudRepository<SizeType, Integer> {
}
