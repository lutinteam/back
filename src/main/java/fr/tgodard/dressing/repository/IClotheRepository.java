package fr.tgodard.dressing.repository;

import fr.tgodard.dressing.beans.Clothe;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface IClotheRepository extends CrudRepository<Clothe, Integer> {

    @Query("SELECT c FROM Clothe c  LEFT JOIN FETCH c.firstColor" +
            "                       LEFT JOIN FETCH c.secondColor" +
            "                       LEFT JOIN FETCH c.category" +
            "                       LEFT JOIN FETCH c.photos" +
            "                       LEFT JOIN FETCH c.size" +
            "                       LEFT JOIN FETCH c.subCategory")
    Iterable<Clothe> findAllWithInfos();

    @Query("SELECT DISTINCT c.brand FROM Clothe c")
    Iterable<String> findAllBrands();

    @Query("SELECT c FROM Clothe c  LEFT JOIN FETCH c.firstColor" +
            "                       LEFT JOIN FETCH c.secondColor" +
            "                       LEFT JOIN FETCH c.category" +
            "                       LEFT JOIN FETCH c.photos" +
            "                       LEFT JOIN FETCH c.size" +
            "                       LEFT JOIN FETCH c.subCategory" +
            "    WHERE c.id = :id")
    Clothe findClotheByIdWithInfos(Integer id);
}
