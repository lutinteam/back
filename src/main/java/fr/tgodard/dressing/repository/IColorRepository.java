package fr.tgodard.dressing.repository;

import fr.tgodard.dressing.beans.Color;
import org.springframework.data.repository.CrudRepository;

public interface IColorRepository extends CrudRepository<Color, Integer> {
}
