package fr.tgodard.dressing.repository;

import fr.tgodard.dressing.beans.Size;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ISizeRepository extends CrudRepository<Size, Integer> {

    @Query("SELECT DISTINCT s FROM Size s LEFT JOIN FETCH s.sizeType")
    Iterable<Size> findAllWithSizeType();
}
