package fr.tgodard.dressing.repository;

import fr.tgodard.dressing.beans.Pattern;
import org.springframework.data.repository.CrudRepository;

public interface IPatternRepository extends CrudRepository<Pattern, Integer> {
}
