package fr.tgodard.dressing.repository;

import fr.tgodard.dressing.beans.SubCategory;
import org.springframework.data.repository.CrudRepository;

public interface ISubCategoryRepository extends CrudRepository<SubCategory, Integer> {
}
