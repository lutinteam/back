package fr.tgodard.dressing.repository;

import fr.tgodard.dressing.beans.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ICategoryRepository extends CrudRepository<Category, Integer> {

    @Query("SELECT DISTINCT c FROM Category c LEFT JOIN FETCH c.subCategories")
    Iterable<Category> findAllWithSubCategoriesAndSizeType();

}
