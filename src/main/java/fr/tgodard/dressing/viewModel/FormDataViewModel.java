package fr.tgodard.dressing.viewModel;

import fr.tgodard.dressing.beans.Category;
import fr.tgodard.dressing.beans.Color;
import fr.tgodard.dressing.beans.Pattern;
import fr.tgodard.dressing.beans.Size;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FormDataViewModel {

    private List<Category> categories = new ArrayList<>();
    private List<Size> sizes = new ArrayList<>();
    private List<Color> colors = new ArrayList<>();
    private List<String> brands = new ArrayList<>();
    private List<Pattern> patterns = new ArrayList<>();
}
